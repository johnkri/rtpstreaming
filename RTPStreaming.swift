//
//  RTPStreaming.swift
//  RTPStreaming
//
//  Created by John on 17/03/2022.
//

import Foundation

public class RTPStreaming {
    
    static let _shared = RTPStreaming()
    
    class public func sharedInstance() -> RTPStreaming {
        return _shared
    }
    
    public func startStreaming(viewController: UIViewController, hostUDP: String, portUDP: Int) {
        buildDependencies(viewController: viewController, hostUDP: hostUDP, portUDP: portUDP)
    }
    
    private func buildDependencies(viewController: UIViewController, hostUDP: String, portUDP: Int) {
        let screenRecorder: ScreenRecorderDelegate = ScreenRecorder()
        let utilities: UtilitiesDelegate = Utilities()
        let enqueuer: EnqueuerDelegate = Enqueuer()
        let jpegParser: JpegParserDelegate = JpegParser()
        let packetHeader: PacketHeaderDelegate = PacketHeader()
        let network: NetworkDelegate = Network()
        
        screenRecorder.viewController = viewController
        screenRecorder.utilities = utilities
        screenRecorder.enqueuer = enqueuer
        enqueuer.jpegParser = jpegParser
        enqueuer.packetHeader = packetHeader
        packetHeader.network = network
    }
    
}
