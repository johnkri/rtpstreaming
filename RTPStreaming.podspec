#
# Be sure to run `pod lib lint Glory.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RTPStreaming'
  s.version          = '1.0.5'
  s.summary          = 'Framework for device rtp streaming.'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = "https://gitlab.com/johnkri/rtpstreaming"
  s.license      = "MIT"
  s.author           = { 'John Kricorian' => 'kricorianjohn@gmail.com' }
  s.source       = { :git => "git@gitlab.com:johnkri/rtpstreaming.git", :tag => "v#{s.version}" }
  s.source_files     = "RTPStreaming/RTPStreaming/**/*.{h,m,swift}"

  s.ios.deployment_target = '15.0'
  s.swift_version = '5.0'
  s.libraries = 'xml2'
  
end
