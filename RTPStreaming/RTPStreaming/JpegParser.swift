//
//  JpegParser.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation
import CoreMedia

protocol JpegParserDelegate: AnyObject {
    func parse(_ sampleBuffer: CMSampleBuffer, completionHandler: @escaping (RTPPacket) -> ())
}

class JpegParser {
    
    static let _shared = JpegParser()
    
    class open func sharedInstance() -> JpegParser {
        return _shared
    }
            
    private var bandRange: Int = 63
    private let lock = NSLock()
    

    private func getLuminanceTable(bytes: [UInt8]) -> [UInt8] {
        var luminanceTable: [UInt8] = []
        
        let luminanceMarker: [Int] = [0xff, 0xdb, 0x00, 0x43, 0x00]
        let iterations = bytes.count - luminanceMarker.count
        DispatchQueue.concurrentPerform(iterations: iterations) { i in
            var same = true
            for n in 0..<luminanceMarker.count {
                if bytes[i + n] != luminanceMarker[n] {
                    same = false
                    break
                }
            }
            if same {
                lock.sync {
                    luminanceTable = Array(bytes[i + luminanceMarker.count...i + bandRange])
                }
            }
        }
        return luminanceTable
    }
    
    private func getChrominanceTable(bytes: [UInt8]) -> [UInt8] {
        var chrominanceTable: [UInt8] = []
        
        let chrominanceMarker: [Int] = [0xff, 0xdb, 0x00, 0x43, 0x01]
        let iterations = bytes.count - chrominanceMarker.count
        DispatchQueue.concurrentPerform(iterations: iterations) { i in
            var same = true
            for n in 0..<chrominanceMarker.count {
                if bytes[i + n] != chrominanceMarker[n] {
                    same = false
                    break
                }
            }
            if same {
                lock.sync {
                    chrominanceTable = Array(bytes[i + chrominanceMarker.count...i + bandRange])
                }
            }
        }
        return chrominanceTable
    }
    
    private func getScanData(bytes: [UInt8]) -> [UInt8] {
        var scanData: [UInt8] = []

        let startOfScanMarker: [Int] = [0xff, 0xda, 0x00, 0x0C, 0x03]
        let iterations = bytes.count - startOfScanMarker.count
        DispatchQueue.concurrentPerform(iterations: iterations) { i in
            var same = true
            for n in 0..<startOfScanMarker.count {
                if bytes[i + n] != startOfScanMarker[n] {
                    same = false
                    break
                }
            }
            if same {
                lock.sync {
                    scanData = Array(bytes[i + 14..<bytes.count])
                }
            }
        }
        return scanData
    }
    
    private func getRestartInterval(bytes: [UInt8]) -> [UInt8] {
        var restartInterval: [UInt8] = []
        
        let driMarker: [Int] = [0xFF, 0xDD]

        for i in 0...bytes.count - driMarker.count {
            var same = true
            for n in 0..<driMarker.count {
                if bytes[i + n] != driMarker[n] {
                    same = false
                    break
                }
            }
            if same {
                var value = 0
                value += Int(bytes[i + 2] << 8)
                value += Int(bytes[i + 3])
                restartInterval = Array(bytes[i + 4..<(i + value + 2)])
            }
        }
        return restartInterval
    }
    
    
    private func getTable(bytes: [UInt8], marker: [Int]) -> [UInt8] {
        var table: [UInt8] = []

        let iterations = bytes.count - marker.count
        DispatchQueue.concurrentPerform(iterations: iterations) { i in
            var same = true
            for n in 0..<marker.count {
                if bytes[i + n] != marker[n] {
                    same = false
                    break
                }
            }
            if same {
                lock.sync {
                    table = Array(bytes[i + 14..<bytes.count])
                }
            }
        }
        return table
    }
}

extension JpegParser: JpegParserDelegate {
    
    internal func parse(_ sampleBuffer: CMSampleBuffer, completionHandler: @escaping (RTPPacket) -> ()) {
        guard let image = sampleBuffer.image, let data = image.jpegData(compressionQuality: 0.5) else { return }
        let bytes = data.bytesArray
        
        let luminanceTable = getLuminanceTable(bytes: bytes)
        let chrominanceTable = getChrominanceTable(bytes: bytes)
        let scanData = getScanData(bytes: bytes)
        let restartInterval = getRestartInterval(bytes: bytes)

        let width = (Int(image.size.width) / 8).toUInt8()
        let height = (Int(image.size.height) / 8).toUInt8()
        
        completionHandler(RTPPacket(luminanceTable: luminanceTable,
                                    chrominanceTable: chrominanceTable,
                                    scanData: scanData,
                                    restartInterval: restartInterval,
                                    width: width,
                                    height: height))
    }
}
