//
//  RTPPacket.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation

class RTPPacket {
    
    internal var luminanceTable: [UInt8]
    internal var chrominanceTable: [UInt8]
    internal var scanData: [UInt8]
    internal var restartInterval: [UInt8]
    internal var width: UInt8
    internal var height: UInt8
    
    internal init(luminanceTable: [UInt8], chrominanceTable: [UInt8], scanData: [UInt8], restartInterval: [UInt8], width: UInt8, height: UInt8) {
        self.luminanceTable = luminanceTable
        self.chrominanceTable = chrominanceTable
        self.scanData = scanData
        self.restartInterval = restartInterval
        self.width = width
        self.height = height
    }
}
