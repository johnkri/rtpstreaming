//
//  NSLocking+Extension.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation

extension NSLocking {
    func sync<T>(_ closure: () throws -> T) rethrows -> T {
        lock()
        defer { unlock() }
        return try closure()
    }
}
