//
//  Enqueur.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation
import CoreMedia

protocol EnqueuerDelegate: AnyObject {
    var sampleBuffer: CMSampleBuffer? { get set }
    var packetHeader: PacketHeaderDelegate? { get set }
    var jpegParser: JpegParserDelegate? { get set }
}

class Enqueuer: EnqueuerDelegate {
        
    internal var packetHeader: PacketHeaderDelegate?
    internal var jpegParser: JpegParserDelegate?
    internal var sampleBuffer: CMSampleBuffer?
    
    private var sampleQueue = Queue<CMSampleBuffer>()

    internal init() {
        updateSampleQueue()
    }
    
    private func parseJpegData(_ sampleBuffer: CMSampleBuffer) {
        jpegParser?.parse(sampleBuffer) { rtpPacket in
            let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
            dispatchQueue.async {
                self.packetHeader?.createHeader(rtpPacket: rtpPacket)
            }
        }
    }
        
    private func updateSampleQueue() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            if let sampleBuffer = self.sampleBuffer {
                self.sampleQueue.enqueue(sampleBuffer)
                if let sampleBuffer = self.sampleQueue.head {
                    self.parseJpegData(sampleBuffer)
                    _ = self.sampleQueue.dequeue()
                }
            }
            self.updateSampleQueue()
        }
    }
}


