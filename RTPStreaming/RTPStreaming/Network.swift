//
//  Network.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation
import Network

protocol NetworkDelegate: AnyObject {
    func sendUDP(_ content: Data)
}

class Network {
    
    var connection: NWConnection?
//    var hostUDP: NWEndpoint.Host = "172.20.10.15"
//        var hostUDP: NWEndpoint.Host = "192.168.12.1"

    var hostUDP: String
    var portUDP: UInt16
    internal init(hostUDP: String, portUDP: UInt16) {
        self.hostUDP = hostUDP
        self.portUDP = portUDP
        var x = 0
        while(x<1000000000) {
            x+=1
        }
        connectToUDP(hostUDP: hostUDP, portUDP: portUDP)
    }

    func connectToUDP(hostUDP: String, portUDP: UInt16) {
        let host: NWEndpoint.Host = NWEndpoint.Host(hostUDP)
        let port: NWEndpoint.Port = NWEndpoint.Port(rawValue: portUDP) ?? NWEndpoint.Port(integerLiteral: .zero)
        self.connection = NWConnection(host: host, port: port, using: .udp)

        self.connection?.stateUpdateHandler = { (newState) in
            print("This is stateUpdateHandler:")
            switch (newState) {
                case .ready:
                    print("State: Ready\n")
                    self.receiveUDP()
                case .setup:
                    print("State: Setup\n")
                case .cancelled:
                    print("State: Cancelled\n")
                case .preparing:
                    print("State: Preparing\n")
                default:
                    print("ERROR! State not defined!\n")
            }
        }

        self.connection?.start(queue: .global())
    }



    func receiveUDP() {
        self.connection?.receiveMessage { (data, context, isComplete, error) in
            if (isComplete) {
                print("Receive is complete")
                if (data != nil) {
                    let backToString = String(decoding: data!, as: UTF8.self)
                    print("Received message: \(backToString)")
                } else {
                    print("Data == nil")
                }
            }
        }
    }
}

extension Network: NetworkDelegate {
    
    func sendUDP(_ content: Data) {
        connection?.send(content: content, completion: NWConnection.SendCompletion.contentProcessed(({ (NWError) in
            if (NWError == nil) {
                print("Data was sent to UDP")
            } else {
                print("ERROR! Error when data (Type: Data) sending. NWError: \n \(NWError!)")
            }
        })))
    }
}

