//
//  Int+Extension.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation

extension Int {
    
    func toUInt8() -> UInt8 {
        let int8: UInt8? = withUnsafeBytes(of: self) { ptr -> UInt8? in
            let binded = ptr.bindMemory(to: UInt8.self)
            return binded.first
        }
        return int8 ?? UInt8(0)
    }
}
