//
//  CMSampleBuffer+Extension.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//


import CoreMedia
import UIKit

extension CMSampleBuffer {
    
    var sampleSize: Int {
        CMSampleBufferGetTotalSampleSize(self)
    }
    
    var toUInt8Array: [UInt8] {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(self) else { return [] }

        CVPixelBufferLockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))
        let byterPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)
        let height = CVPixelBufferGetHeight(imageBuffer)
        guard let srcBuff = CVPixelBufferGetBaseAddress(imageBuffer) else { return [] }
        let data = Data(bytes: srcBuff, count: byterPerRow * height)
        CVPixelBufferUnlockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))

        return [UInt8].init(repeating: 0, count: data.count / MemoryLayout<UInt8>.size)
    }
    
    var toData: Data? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(self) else { return nil }
        CVPixelBufferLockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))
        
        let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)
        let height = CVPixelBufferGetHeight(imageBuffer)
        guard let src_buff = CVPixelBufferGetBaseAddress(imageBuffer) else { return nil }
        let data = Data(bytes: src_buff, count: bytesPerRow * height)
        
        CVPixelBufferUnlockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))
        
        return data
    }
    
    var image: UIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(self) else { return nil }
        let ciimage = CIImage(cvPixelBuffer: imageBuffer)
        let context = CIContext(options: nil)
        let cgImage = context.createCGImage(ciimage, from: ciimage.extent)!
        return UIImage(cgImage: cgImage)
    }
}

