//
//  Queue.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation

struct Queue<T> {
  private var elements: [T] = []

  mutating func enqueue(_ value: T) {
    elements.append(value)
  }

  mutating func dequeue() -> T? {
    guard !elements.isEmpty else {
      return nil
    }
    return elements.removeFirst()
  }

  var head: T? {
    return elements.first
  }

  var tail: T? {
    return elements.last
  }
}

