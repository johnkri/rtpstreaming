//
//  UIView+Extension.swift
//  RTPStreaming
//
//  Created by John on 16/03/2022.
//

import UIKit

extension UIApplication {
    var keyWindow: UIWindow? {
        UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first 
    }
}
