//
//  Wireframe.swift
//  RTPStreaming
//
//  Created by John on 17/03/2022.
//

import Foundation

protocol WireframeDelegate {
    
}

final public class Wireframe {
    
    static let _sharedInstance = Wireframe()
    
    public class func shared() -> Wireframe {
        return _sharedInstance
    }
    
    func buildDependencies(viewController: UIViewController, hostUDP: String, portUDP: UInt16) {
        let screenRecorder: ScreenRecorderDelegate = ScreenRecorder()
        let utilities: UtilitiesDelegate = Utilities()
        let enqueuer: EnqueuerDelegate = Enqueuer()
        let jpegParser: JpegParserDelegate = JpegParser()
        let packetHeader: PacketHeaderDelegate = PacketHeader()
        let network: NetworkDelegate = Network(hostUDP: hostUDP, portUDP: portUDP)
        
        screenRecorder.utilities = utilities
        screenRecorder.enqueuer = enqueuer
        enqueuer.packetHeader = packetHeader
        enqueuer.jpegParser = jpegParser
        packetHeader.network = network
        
        screenRecorder.startRecording(viewController: viewController)
    }
    
    public func startStreaming(viewController: UIViewController, hostUDP: String, portUDP: UInt16) {
        buildDependencies(viewController: viewController, hostUDP: hostUDP, portUDP: portUDP)
    }
}

