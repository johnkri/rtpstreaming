//
//  Enqueur.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation
import CoreMedia

@objc class Enqueuer: NSObject {
    
    static let _shared = Enqueuer()
    
    @objc open class func sharedInstance() -> Enqueuer {
        return _shared
    }
    
    internal var packetHeader = PacketHeader.sharedInstance()
    internal var jpegParser = JpegParser.sharedInstance()

    @objc internal var sampleBuffer: CMSampleBuffer?
    
    private var sampleQueue = Queue<CMSampleBuffer>()

    internal override init() {
        super.init()
        updateSampleQueue()
    }
    
    private func parseJpegData(_ sampleBuffer: CMSampleBuffer) {
        jpegParser.parse(sampleBuffer) { rtpPacket in
            let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
            dispatchQueue.async {
                self.packetHeader.createHeader(rtpPacket: rtpPacket)
            }
        }
    }
        
    private func updateSampleQueue() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            if let sampleBuffer = self.sampleBuffer {
                self.sampleQueue.enqueue(sampleBuffer)
                if let sampleBuffer = self.sampleQueue.head {
                    self.parseJpegData(sampleBuffer)
                    _ = self.sampleQueue.dequeue()
                }
            }
            self.updateSampleQueue()
        }
    }
}


