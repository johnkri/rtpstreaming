//
//  ScreenRecorder.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 04/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation
import UIKit
import AVKit

enum VideoScreenRecorderError: Error {
    case alreadyRecoding
    case noRecordInProgress
    case creationFailed
}

public final class ScreenRecorder {
    public static let shared = ScreenRecorder()
    
    internal weak var viewController: UIViewController?

    fileprivate(set) var isRecording = false
    fileprivate var videoURL: URL!

    // Windows added to this array will not be recorded by the recorder
    // convenient if there is a window containing the Start/Stop buttons
    // that you do not wish to include in the video
    fileprivate var escapeWindows: [UIWindow] = []

    fileprivate var videoWriter: AVAssetWriter!
    fileprivate var videoWriterInput: AVAssetWriterInput!
    fileprivate var avAdaptor: AVAssetWriterInputPixelBufferAdaptor!
    fileprivate var displayLink: CADisplayLink!
    fileprivate var firstTimeStamp: CFTimeInterval?
    fileprivate var outputBufferPoolAuxAttributes: [Any] = []

    fileprivate var renderQueue: DispatchQueue
    fileprivate var appendPixelBufferQueue: DispatchQueue
    fileprivate var frameRenderingSemaphore: DispatchSemaphore
    fileprivate var pixelAppendSemaphore: DispatchSemaphore

    fileprivate var viewSize: CGSize = UIApplication.shared.keyWindow?.frame.size ?? UIScreen.main.bounds.size
    fileprivate var scale: CGFloat = UIScreen.main.scale

    fileprivate var outputBufferPool: CVPixelBufferPool?

    // MARK: - Public

    public func startRecording(viewController: UIViewController) {
        self.viewController = viewController
        self.setupWriter()
        self.isRecording = self.videoWriter!.status == .writing
        self.displayLink = CADisplayLink(target: self, selector: #selector(writeVideoFrame))
        self.displayLink?.add(to: RunLoop.main, forMode: .common)
    }

    func stopRecording(completion: ((URL?, Error?) -> Void)? = nil) {
        guard self.isRecording else {
            completion?(nil, VideoScreenRecorderError.noRecordInProgress)
            return
        }
        self.isRecording = false
        self.displayLink?.remove(from: RunLoop.main, forMode: .common)
        self.completeRecordingSession(completion: completion)
    }

    // MARK: - Private
    private init() {
        self.appendPixelBufferQueue = DispatchQueue(label: "ASScreenRecorder.append_queue")
        self.renderQueue = DispatchQueue(label: "ASScreenRecorder.render_queue", qos: .userInitiated)

        self.frameRenderingSemaphore = DispatchSemaphore(value: 1)
        self.pixelAppendSemaphore = DispatchSemaphore(value: 1)

        if UIDevice.current.userInterfaceIdiom == .pad && self.scale > 1 {
            self.scale = 1.0
        }
    }

    fileprivate func setupWriter() {
        let bufferAttributes: [CFString: Any] = [kCVPixelBufferPixelFormatTypeKey: kCVPixelFormatType_32BGRA,
                                                                                         kCVPixelBufferCGBitmapContextCompatibilityKey: true,
                                                                                         kCVPixelBufferWidthKey: viewSize.width * scale,
                                                                                         kCVPixelBufferHeightKey: viewSize.height * scale,
                                                                                         kCVPixelBufferBytesPerRowAlignmentKey: viewSize.width * scale * 4]

        self.outputBufferPool = nil

        CVPixelBufferPoolCreate(nil, nil, bufferAttributes as CFDictionary, &self.outputBufferPool)
         videoURL = URL(string: "")

        do {
            try self.videoWriter = AVAssetWriter(url: self.tempFileURL()!, fileType: AVFileType.mov)
        } catch let error {
            print(error)
        }

        let pixelNumber = viewSize.width * viewSize.height * scale
        let videoCompression = [AVVideoAverageBitRateKey: pixelNumber * 11.4]

        let videoSettings: [String: Any] = [AVVideoCodecKey: AVVideoCodecType.h264,
                                                                                AVVideoWidthKey: Int(viewSize.width * scale),
                                                                                AVVideoHeightKey: Int(viewSize.height * scale),
                                                                                AVVideoCompressionPropertiesKey: videoCompression]

        videoWriterInput = AVAssetWriterInput(mediaType: .video, outputSettings: videoSettings)
        videoWriterInput.expectsMediaDataInRealTime = true
        videoWriterInput.transform = videoTransformForDeviceOrientation

        avAdaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: videoWriterInput, sourcePixelBufferAttributes: nil)

        videoWriter.add(videoWriterInput)
        videoWriter.startWriting()
        videoWriter.startSession(atSourceTime: CMTime(seconds: 0.0, preferredTimescale: 1000))
    }
    
    func tempFileURL() -> URL? {
        let outputPath = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("tmp/screenCapture.mp4").path
        removeTempFilePath(outputPath)
        return URL(fileURLWithPath: outputPath)
    }
    
    func removeTempFilePath(_ filePath: String?) {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath ?? "") {
            do {
                 try fileManager.removeItem(atPath: filePath ?? "")
            } catch {
                
            }
        }
    }

    fileprivate var videoTransformForDeviceOrientation: CGAffineTransform {
        switch UIDevice.current.orientation {
        case .landscapeLeft:
            return CGAffineTransform(rotationAngle: -CGFloat.pi / 2.0)
        case .landscapeRight:
            return CGAffineTransform(rotationAngle: CGFloat.pi / 2.0)
        case .portraitUpsideDown:
            return CGAffineTransform(rotationAngle: CGFloat.pi)
        default:
            return .identity
        }
    }

    @objc fileprivate func writeVideoFrame() {
        // throttle the number of frames to prevent meltdown
        // technique gleaned from Brad Larson's answer here: http://stackoverflow.com/a/5956119
        if self.frameRenderingSemaphore.wait(timeout: DispatchTime.now()) != .success {
            return
        }
        self.renderQueue.async {
            if !self.videoWriterInput.isReadyForMoreMediaData {
                return
            }

            if self.firstTimeStamp == nil {
                self.firstTimeStamp = self.displayLink.timestamp
            }
            let elapsed: CFTimeInterval = self.displayLink.timestamp - self.firstTimeStamp!
            let time = CMTimeMakeWithSeconds(elapsed, preferredTimescale: 1000)

            var unmanagedPixelBuffer: CVPixelBuffer? = nil
            let bitmapContext = self.createPixelBufferAndBitmapContext(pixelBuffer: &unmanagedPixelBuffer)

            // Draw each window into the context (other windows include UIKeyboard, UIAlert)
            // FIX: UIKeyboard is currently only rendered correctly in portrait orientation
            DispatchQueue.main.sync {
                UIGraphicsPushContext(bitmapContext)
                guard let viewController = self.viewController else {
                    return
                }
                viewController.view.drawHierarchy(in: CGRect(x: 0.0, y: 0.0, width: viewController.view.frame.size.width, height: viewController.view.frame.size.height), afterScreenUpdates: false)
                UIGraphicsPopContext()
            }

            // Append pixelBuffer on a async dispatch_queue, the next frame is rendered whilst this one appends
            // must not overwhelm the queue with pixelBuffers, therefore:
            // check if _append_pixelBuffer_queue is ready
            // if it’s not ready, release pixelBuffer and bitmapContext
            if self.pixelAppendSemaphore.wait(timeout: DispatchTime.now()) == .success {
                self.appendPixelBufferQueue.async {
                    let success = self.avAdaptor.append(unmanagedPixelBuffer!, withPresentationTime: time)
                    if !success {
                        print("Warning, unable to write buffer to video")
                    }
                    self.sendPixelBuffer(pixelBuffer: unmanagedPixelBuffer!)
                    CVPixelBufferUnlockBaseAddress(unmanagedPixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
                    self.pixelAppendSemaphore.signal()
                }
            } else {
                CVPixelBufferUnlockBaseAddress(unmanagedPixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
            }
            self.frameRenderingSemaphore.signal()
        }
    }
    
    fileprivate func sendPixelBuffer(pixelBuffer: CVPixelBuffer) {
        let utils = Utilities.sharedInstance()
        let sampleBuffer = utils.createSampleBufferFrom(pixelBuffer: pixelBuffer)
        let enqueuer = Enqueuer.sharedInstance()
        enqueuer.sampleBuffer = sampleBuffer
    }

    fileprivate func createPixelBufferAndBitmapContext(pixelBuffer: UnsafeMutablePointer<CVPixelBuffer?>) -> CGContext {
        CVPixelBufferPoolCreatePixelBuffer(nil, self.outputBufferPool!, pixelBuffer)
        CVPixelBufferLockBaseAddress(pixelBuffer.pointee!, CVPixelBufferLockFlags(rawValue: 0))

        let bitmapInfo = CGBitmapInfo(rawValue: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        let context = CGContext(data: CVPixelBufferGetBaseAddress(pixelBuffer.pointee!),
                                                        width: CVPixelBufferGetWidth(pixelBuffer.pointee!),
                                                        height: CVPixelBufferGetHeight(pixelBuffer.pointee!),
                                                        bitsPerComponent: 8,
                                                        bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer.pointee!),
                                                        space: CGColorSpaceCreateDeviceRGB(),
                                                        bitmapInfo: bitmapInfo.rawValue)!

        context.scaleBy(x: self.scale, y: self.scale)
        let flipVertical = CGAffineTransform(a: 1.0, b: 0.0, c: 0.0, d: -1.0, tx: 0.0, ty: self.viewSize.height)
        context.concatenate(flipVertical)

        return context
    }

    fileprivate func completeRecordingSession(completion: ((URL?, Error?) -> Void)? = nil) {
        self.renderQueue.async {
            self.appendPixelBufferQueue.async {
                self.videoWriterInput.markAsFinished()
                self.videoWriter.finishWriting {
                    self.cleanup()
                    DispatchQueue.main.async {
                        completion?(self.videoURL, nil)
                    }
                }
            }
        }
    }

    fileprivate func cleanup() {
        self.avAdaptor = nil
        self.videoWriterInput = nil
        self.videoWriter = nil
        self.firstTimeStamp = nil
        self.outputBufferPoolAuxAttributes.removeAll()
        self.escapeWindows.removeAll()
    }
}

