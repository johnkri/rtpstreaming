//
//  JpegHeader.swift
//  RTPPlayer
//
//  Created by John Kricorian on 21/01/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import Foundation

public class JpegHeader {

    var fragmentOffset: Int
    var width: UInt8
    var height: UInt8
    
    internal init(fragmentOffset: Int, width: UInt8, height: UInt8) {
        self.fragmentOffset = fragmentOffset
        self.width = width
        self.height = height
    }
        
    /** Type header. */
    let TYPE_SPECIFIC: UInt8 = 0
    /** Header type. */
    let TYPE: UInt8 = 65 
    /** Header Q. */
    let Q: UInt8 = 255
    
    var header: [UInt8] {
        var output: [UInt8] = []
        output.append(TYPE_SPECIFIC)
        output.append((fragmentOffset >> 16).toUInt8())
        output.append((fragmentOffset >> 8).toUInt8())
        output.append(fragmentOffset.toUInt8() & 0xFF)
        output.append(TYPE)
        output.append(Q)
        output.append(width)
        output.append(height)
        
        return output
    }
}
