//
//  Network.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation
import Network

class Network {
    
    static var _shared = Network()
    
    class open func sharedInstance() -> Network {
        return _shared
    }
        
    var connection: NWConnection?
//    var hostUDP: NWEndpoint.Host = "172.20.10.15"
//        var hostUDP: NWEndpoint.Host = "192.168.12.1"
    var hostUDP: NWEndpoint.Host = "172.20.42.42"
    var portUDP: NWEndpoint.Port = 5001

    private init() {
        var x = 0
        while(x<1000000000) {
            x+=1
        }
        connectToUDP(hostUDP: hostUDP, portUDP: portUDP)
    }

    func connectToUDP(hostUDP: NWEndpoint.Host, portUDP: NWEndpoint.Port) {
        self.connection = NWConnection(host: hostUDP, port: portUDP, using: .udp)

        self.connection?.stateUpdateHandler = { (newState) in
            print("This is stateUpdateHandler:")
            switch (newState) {
                case .ready:
                    print("State: Ready\n")
                    self.receiveUDP()
                case .setup:
                    print("State: Setup\n")
                case .cancelled:
                    print("State: Cancelled\n")
                case .preparing:
                    print("State: Preparing\n")
                default:
                    print("ERROR! State not defined!\n")
            }
        }

        self.connection?.start(queue: .global())
    }

    func sendUDP(_ content: Data) {
        connection?.send(content: content, completion: NWConnection.SendCompletion.contentProcessed(({ (NWError) in
            if (NWError == nil) {
                print("Data was sent to UDP")
            } else {
                print("ERROR! Error when data (Type: Data) sending. NWError: \n \(NWError!)")
            }
        })))
    }

    func receiveUDP() {
        self.connection?.receiveMessage { (data, context, isComplete, error) in
            if (isComplete) {
                print("Receive is complete")
                if (data != nil) {
                    let backToString = String(decoding: data!, as: UTF8.self)
                    print("Received message: \(backToString)")
                } else {
                    print("Data == nil")
                }
            }
        }
    }
}

