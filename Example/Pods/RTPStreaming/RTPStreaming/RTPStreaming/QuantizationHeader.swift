//
//  QuantizationHeader.swift
//  RTPPlayer
//
//  Created by John Kricorian on 21/01/2022.
//  Copyright © 2022 Smartbox. All rights reserved.
//

import Foundation

/** Quantization header static values. */
class QuantizationHeader {
        
    var MBZ: UInt8 = 0
    var PRECISION: UInt8 = 0
    var quantizationTable: [UInt8]
    var fragmentOffset: Int
    var width: UInt8
    var height: UInt8
    
    init(quantizationTable: [UInt8], fragmentOffset: Int, width: UInt8, height: UInt8) {
        self.quantizationTable = quantizationTable
        self.fragmentOffset = fragmentOffset
        self.width = width
        self.height = height
    }
    
    var header: [UInt8] {
        var header: [UInt8] = []
        header.append(MBZ)
        header.append(PRECISION)
        header.append(UInt8(quantizationTable.count >> 8))
        header.append(UInt8(quantizationTable.count))
        header.append(contentsOf: quantizationTable)
        
        return header
    }
}
