//
//  PacketHeader.swift
//  ExampleRecorder
//
//  Created by John Kricorian on 01/03/2022.
//  Copyright © 2022 Alan Skipp. All rights reserved.
//

import Foundation

class PacketHeader {
    
    static let _shared = PacketHeader()
    
    class open func sharedInstance() -> PacketHeader {
        return _shared
    }
        
    internal var network = Network.sharedInstance()
    
    private var sequenceNumber = 0
    private var timeStamp = 0
    private var JPEG_FREQUENCY = 90000
    
}

extension PacketHeader {
    
    internal func createHeader(rtpPacket: RTPPacket) {
        let MAX_RTP_CONTENT_SIZE = 1280
        var fragmentOffset = 0
        
        while (fragmentOffset < rtpPacket.scanData.count) {
            let payloadSize = min(MAX_RTP_CONTENT_SIZE, rtpPacket.scanData.count - fragmentOffset);
            let marker = UInt8(fragmentOffset + payloadSize >= rtpPacket.scanData.count ? 1 : 0) // Determine end of packet
            // Get a part of rtp content, split image in multiple rtp packets.
            let payload = rtpPacket.scanData[fragmentOffset..<fragmentOffset + payloadSize]
            
            let rtpHeader = RTPHeader(marker: marker, sequenceNumber: sequenceNumber, timeStamp: timeStamp).header
            let jpegHeader = JpegHeader(fragmentOffset: fragmentOffset, width: rtpPacket.width, height: rtpPacket.height).header
            let restartMarkerHeader = RestartMarkerHeader(restartInterval: rtpPacket.restartInterval).header
            
            var packet = rtpHeader + jpegHeader + restartMarkerHeader
            
            if fragmentOffset == 0 { // Start of packet
                let quantizationTable = rtpPacket.luminanceTable + rtpPacket.chrominanceTable
                let quantizationHeader = QuantizationHeader(quantizationTable: quantizationTable, fragmentOffset: fragmentOffset, width: rtpPacket.width, height: rtpPacket.height).header
                packet.append(contentsOf: quantizationHeader)
            }
            packet.append(contentsOf: payload)
            network.sendUDP(Data(packet))
            
            sequenceNumber += 1;
            fragmentOffset += payloadSize
        }
        timeStamp += (JPEG_FREQUENCY)
    }
}
