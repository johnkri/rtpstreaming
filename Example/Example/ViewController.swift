//
//  ViewController.swift
//  Example
//
//  Created by John on 16/03/2022.
//

import UIKit
import RTPStreaming
import AVFoundation

class ViewController: UIViewController {
    
    let wireframe = Wireframe.shared()
    private var player = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wireframe.startStreaming(viewController: self, hostUDP: "172.20.42.42", portUDP: 5001)
        let sound = Bundle.main.path(forResource: "FeelinGood", ofType: "mp3")!

        do {
            player = try AVPlayer(url: URL(fileURLWithPath: sound))
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)
        } catch let error {
            print(error.localizedDescription)
        }
        player.play()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let secondViewController = SecondViewController()
        secondViewController.delegate = self
        secondViewController.modalPresentationStyle = .overFullScreen
        present(secondViewController, animated: true)
    }
}

extension ViewController: SecondViewDelegate {
    func updateBackgroundColor() {
        view.backgroundColor = UIColor.random()
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(
           red:   .random(),
           green: .random(),
           blue:  .random(),
           alpha: 1.0
        )
    }
}
