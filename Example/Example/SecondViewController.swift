//
//  SecondViewController.swift
//  Example
//
//  Created by John on 18/03/2022.
//

import UIKit

protocol SecondViewDelegate: AnyObject {
    func updateBackgroundColor()
}

class SecondViewController: UIViewController {
    
    var delegate: SecondViewDelegate?
    
    @IBAction func updateColorButtonPressed(sender: UIButton) {
        delegate?.updateBackgroundColor()
    }

    override func viewDidLoad() {
        super.viewDidLoad()


    }

}
